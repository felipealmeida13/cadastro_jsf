package br.com.jpa.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.com.jpa.model.Pessoa;
import br.com.jpa.model.PessoaBuilder;

//TODO Implementar teste unit�rio do m�todo adicionar();

public class PessoaMBTest {
	
	private PessoaMB PessoaDAOMock;
	String pessoaEsperada = ("paginas/Sucesso");
	Pessoa pessoa = PessoaBuilder.aPessoa().build();
	
	@Test
	public void adicionarTest() {
		PessoaMB pessoaMB = new PessoaMB();
		pessoaMB.setPessoa(pessoa);
		
		String pessoaAtual = new PessoaMB().adicionar();
		Assertions.assertEquals(pessoaEsperada, pessoaAtual);
	}
	@Test
	public void adicionarMock() {
		PessoaDAOMock = Mockito.mock(PessoaMB.class);
		
		Mockito.when(PessoaDAOMock.adicionar()).thenReturn(pessoaEsperada);
	}
	
}
