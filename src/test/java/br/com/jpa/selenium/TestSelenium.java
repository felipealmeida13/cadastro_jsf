package br.com.jpa.selenium;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class TestSelenium {
	private static WebDriver driver;
	
	@BeforeAll
	public static void setup() {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		driver = new ChromeDriver();
	}
	@Test
	public void testarCadastro() throws InterruptedException{
		driver.get("http://localhost:8080/my-project/Cadastro.xhtml");

		
		WebElement preencherQuestNome = driver.findElement(By.name("j_idt6:j_idt9"));
		preencherQuestNome.sendKeys("Ada Lovelace");
		
		WebElement preencherQuestSenha = driver.findElement(By.name("j_idt6:j_idt11"));
		preencherQuestSenha.sendKeys("123");
		
		WebElement selecionarStatus = driver.findElement(By.id("j_idt6:j_idt17:0"));
		selecionarStatus.click();
		
		WebElement preencherQuestObs = driver.findElement(By.name("j_idt6:j_idt21"));
		preencherQuestObs.sendKeys("Programadora" + Keys.ENTER);
		
		WebElement clicarNoCadastrar = driver.findElement(By.id("j_idt6:j_idt22"));
		clicarNoCadastrar.click();

		Thread.sleep(5000);
		String conteudo = driver.getPageSource();

		Assertions.assertTrue(conteudo.contains(""));
	}

}
