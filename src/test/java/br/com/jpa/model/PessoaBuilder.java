package br.com.jpa.model;

public class PessoaBuilder {

	public static final Integer DEFAULT_ID = 01;
	public static final String DEFAULT_NOME = "Ada Lovelace";
	public static final String DEFAULT_EMAIL = "adalove@gmail.com";
	public static final String DEFAULT_ROLE = "ROLE_PESSOA";

	private Integer id = DEFAULT_ID;
	private String nome = DEFAULT_NOME;
	private String email = DEFAULT_EMAIL;
	private String role = DEFAULT_ROLE;

	public PessoaBuilder() {
	}

	public static PessoaBuilder aPessoa() {
		return new PessoaBuilder();
	}

	public PessoaBuilder withId(Integer id) {
		this.id = id;
		return this;
	}

	public PessoaBuilder withNome(String nome) {
		this.nome = nome;
		return this;
	}

	public PessoaBuilder withEmail(String email) {
		this.email = email;
		return this;
	}

	public PessoaBuilder inPessoaRole() {
		this.role = "ROLE_USER";
		return this;
	}

	public PessoaBuilder inRole(String role) {
		this.role = role;
		return this;
	}

	public PessoaBuilder but() {
		return PessoaBuilder.aPessoa().inRole(role).withId(id).withNome(nome).withEmail(email);
	}

	public Pessoa build() {
		Pessoa pessoa = new Pessoa();
		pessoa.setId(id);
		pessoa.setNome(nome);
		pessoa.setEmail(email);
		return pessoa;
	}

}
