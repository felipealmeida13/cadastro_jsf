package br.com.jpa.model;

import java.util.ArrayList;
import java.util.List;

import br.com.jpa.controller.PessoaMB;

public class PessoaMBBuilder {
	public static final Pessoa DEFAULT_PESSOA = PessoaBuilder.aPessoa().build();
	public static final List<Pessoa> DEFAULT_PESSOAS = new ArrayList<>();
	public static final String DEFAULT_ROLE = "ROLE_PESSOAS";

	private Pessoa pessoa = DEFAULT_PESSOA;
	private List<Pessoa> pessoas = DEFAULT_PESSOAS;
	private String role = DEFAULT_ROLE;

	private PessoaMBBuilder() {
	}

	public static PessoaMBBuilder aPessoaMB() {
		return new PessoaMBBuilder();
	}

	public PessoaMBBuilder withPessoaMB(Pessoa pessoa) {
		this.pessoa = pessoa;
		return this;
	}

	public PessoaMBBuilder withPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
		return this;
	}

	public PessoaMBBuilder inPessoaRole() {
		this.role = "ROLE_USER";
		return this;
	}

	public PessoaMBBuilder inRole(String role) {
		this.role = role;
		return this;
	}

	public PessoaMBBuilder but() {
		return PessoaMBBuilder.aPessoaMB().inRole(role).withPessoaMB(pessoa).withPessoas(pessoas);
	}

	public PessoaMB build() {
		PessoaMB pessoaMB = new PessoaMB();
		pessoaMB.setPessoa(pessoa);
		pessoaMB.setPessoas(pessoas);;
		return pessoaMB;
	}

}
