package br.com.jpa.model;

public class RegistroNaoEncontrado extends Exception {

	private static final long serialVersionUID = 1L;

	public RegistroNaoEncontrado(String message) {
		super(message);
	}

}
